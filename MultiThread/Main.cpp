/*
 * Main.cpp
 *
 *  Created on: 09/10/2018
 *      Author: student
 */
#include <CImg.h>
#include <math.h>
#include <stdio.h>
#include <pthread.h>
#include <errno.h>
#include <string.h>
using namespace cimg_library;

int width, height;												// Width and height of the image
double *pRcomp, *pGcomp, *pBcomp, *pRcomp2, *pGcomp2, *pBcomp2; // Pointers to the R, G and B components
double *pRnew, *pGnew, *pBnew;

void *fusion1(void *vargp)
{
	for (int j = 0; j < 20; j++)
	{
		for (int i = 0; i < (width * height); i++)
		{
			*(pRnew + i) = (255 - ((255 - *(pRcomp + i)) * (255 - *(pRcomp2 + i)) / 255));
		}
	}
	return NULL;
}

void *fusion2(void *vargp)
{
	for (int j = 0; j < 20; j++)
	{
		for (int i = 0; i < (width * height); i++)
		{
			*(pGnew + i) = (255 - ((255 - *(pGcomp + i)) * (255 - *(pGcomp2 + i)) / 255));
		}
	}
	return NULL;
}

void *fusion3(void *vargp)
{
	for (int j = 0; j < 20; j++)
	{
		for (int i = 0; i < (width * height); i++)
		{
			*(pBnew + i) = (255 - ((255 - *(pBcomp + i)) * (255 - *(pBcomp2 + i)) / 255));
		}
	}
	return NULL;
}

int main(int argc, char **argv)
{

	/***************************************************
	 *
	 * Variables inicialization.
	 * Preparation of the necessary elements for the algorithm
	 * Out of the benchmark time
	 *
	 */

	CImg<double> srcImage(argv[1]);  // Open file and object initialization
	CImg<double> srcImage2(argv[2]); // Open file and object initialization

	double *pdstImage; // Pointer to the new image pixels
	int nComp;		   // Number of image components
	struct timespec tStart, tEnd;
	double dElapsedTime;
	srcImage.display(); // If needed, show the source image
	srcImage2.display();
	width = srcImage2.width(); // Getting information from the source image
	height = srcImage2.height();
	nComp = srcImage2.spectrum(); // source image number of components
								  // Common values for spectrum (number of image components):
								  //  B&W images = 1
								  //	Normal color images = 3 (RGB)
								  //  Special color images = 4 (RGB and alpha/transparency channel)

	// Allocate memory space for the pixels of the destination (processed) image
	pdstImage = (double *)malloc(width * height * nComp * sizeof(double));
	if (pdstImage == NULL)
	{
		printf("\nMemory allocating error\n");
		exit(-2);
	}

	// Pointers to the RGB arrays of the source image
	pRcomp = srcImage.data();		  // pRcomp points to the R component
	pGcomp = pRcomp + height * width; // pGcomp points to the G component
	pBcomp = pGcomp + height * width; // pBcomp points to B component

	pRcomp2 = srcImage2.data(); // pRcomp2 points to the R component
	pGcomp2 = pRcomp2 + height * width;
	pBcomp2 = pGcomp2 + height * width;

	// Pointers to the RGB arrays of the destination image
	pRnew = pdstImage;
	pGnew = pRnew + height * width;
	pBnew = pGnew + height * width;

	/*********************************************
	 * Algorithm start
	 *
	 * Take time to measure
	 *
	 */

	printf("Runing task		: ");
	if (clock_gettime(CLOCK_REALTIME, &tStart) == -1)
	{
		//printf("ERROR: clock_gettime: %d.\n", errno);
		exit(EXIT_FAILURE);
	}

	/************************************************
	 *       Algorithm.
	 *
	 */
	pthread_t thread_id;
	pthread_create(&thread_id, NULL, fusion1, NULL);
	pthread_create(&thread_id, NULL, fusion2, NULL);
	pthread_create(&thread_id, NULL, fusion3, NULL);
	pthread_join(thread_id, NULL);

	/***********************************************
	 * End of the algorithm
	 *
	 * Take a new time to measure
	 *
	 * Calculate time spent in the run
	 *
	 */

	if (clock_gettime(CLOCK_REALTIME, &tEnd) == -1)
	{
		//printf("ERROR: clock_gettime: %d.\n", errno);
		exit(EXIT_FAILURE);
	}

	dElapsedTime = (tEnd.tv_sec - tStart.tv_sec);
	dElapsedTime += (tEnd.tv_nsec - tStart.tv_nsec) / 1e+9;
	printf("\nTime spent: %f s\n", dElapsedTime);

	/******************************************************
	 * Store algorithm result in the disk
	 *
	 * Show time spent in the algorithm in the screen
	 *
	 *
	 */

	// Create a new image object with the calculated pixels
	// In case of normal color image use nComp=3,
	// In case of B&W image use nComp=1.

	CImg<double> dstImage(pdstImage, width, height, 1, nComp);

	// Store the destination image in disk
	dstImage.save("output.bmp");

	// Display the destination image
	dstImage.display(); // If needed, show the result image

	return (0);
}
