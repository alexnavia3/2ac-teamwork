/*
 *  Created on: 24 sept. 2019
 *      Author: arias
 */

// Example of use of intrinsic functions
// This example doesn't include any code about image processing
// The image processing code must be added by the students and remove the unnecessary code.

#include <CImg.h>
#include <immintrin.h> // Required to use intrinsic functions
#include <malloc.h>
#include <stdio.h>

using namespace cimg_library;

#define ELEMENTSPERPACKET (sizeof(__m128)/sizeof(double))

int main(int argc, char **argv)
{
    /********************************
     * Here: variable initialization for handling images
     */

    // Data arrays to sum. Might be o not memory aligned to __m128 size (32 bytes)
  	CImg<double> srcImage("hojas_2.bmp");  // Open file and object initialization
		CImg<double> srcImage2("uniovi_2.bmp"); // Open file and object initialization
  	double *pRcomp, *pGcomp, *pBcomp, *pRcomp2, *pGcomp2, *pBcomp2; // Pointers to the R, G and B components
		double *pRnew, *pGnew, *pBnew;
		double *pdstImage; // Pointer to the new image pixels
		int width=srcImage2.width(); // Getting information from the source image
    int height=srcImage2.height(); // Width and height of the image
		int nComp;		   // Number of image components
		struct timespec tStart, tEnd;
		double dElapsedTime;
    int TAM = width*height;
    // Calculation of the size of the results array
    // How many 256 bit packets fit in the array?
    int NPackets = (TAM * sizeof(double)/sizeof(__m128d));
    // If is not a exact number we need to add one more packet
    if ( ((TAM * sizeof(double))%sizeof(__m128d)) != 0)
        NPackets++;
	  // Pointers to the RGB arrays of the source image
		pRcomp = srcImage.data();		  // pRcomp points to the R component
		pGcomp = pRcomp + height * width; // pGcomp points to the G component
		pBcomp = pGcomp + height * width; // pBcomp points to B component

		pRcomp2 = srcImage2.data(); // pRcomp2 points to the R component
		pGcomp2 = pRcomp2 + height * width;
		pBcomp2 = pGcomp2 + height * width;

		// Pointers to the RGB arrays of the destination image
		pRnew = pdstImage;
		pGnew = pRnew + height * width;
		pBnew = pGnew + height * width;

for (int j = 0; j < 20; j++){
    // Create an array aligned to 32 bytes (256 bits) memory boundaries to store the sum.
    // Aligned memory access improves performance  
  
    double *c = (double *)_mm_malloc(sizeof(__m128d) * NPackets, sizeof(__m128d));
    double *r = (double *)_mm_malloc(sizeof(__m128d) * NPackets, sizeof(__m128d));
	  double *r2 = (double *)_mm_malloc(sizeof(__m128d) * NPackets, sizeof(__m128d));
    double *r3 = (double *)_mm_malloc(sizeof(__m128d) * NPackets, sizeof(__m128d));
    double *d1 = (double *)_mm_malloc(sizeof(__m128d) * NPackets, sizeof(__m128d));
    double *m1 = (double *)_mm_malloc(sizeof(__m128d) * NPackets, sizeof(__m128d));
  
    // 32 bytes (256 bits) packets. Used to stored aligned memory data
    __m128d va,vb,vc,vr,vr2,vr3,vd1,vm1;

    // Set the initial c element's value to -1 using vector extensions
    *(__m128d *) c = _mm_set1_pd(255);
    *(__m128d *)(c + ELEMENTSPERPACKET) = _mm_set1_pd(255);
    *(__m128d *)(c + ELEMENTSPERPACKET * 2) = _mm_set1_pd(255);

		*(__m128d *) r = _mm_set1_pd(-1.0);
  	*(__m128d *)(r + ELEMENTSPERPACKET) = _mm_set1_pd(-1.0);
    *(__m128d *)(r+ ELEMENTSPERPACKET * 2) = _mm_set1_pd(-1.0);
  
    *(__m128d *) r2 = _mm_set1_pd(-1.0);
  	*(__m128d *)(r2 + ELEMENTSPERPACKET) = _mm_set1_pd(-1.0);
    *(__m128d *)(r2+ ELEMENTSPERPACKET * 2) = _mm_set1_pd(-1.0);
  
    *(__m128d *) r3 = _mm_set1_pd(-1.0);
  	*(__m128d *)(r3 + ELEMENTSPERPACKET) = _mm_set1_pd(-1.0);
    *(__m128d *)(r3+ ELEMENTSPERPACKET * 2) = _mm_set1_pd(-1.0);
  
    *(__m128d *) d1 = _mm_set1_pd(-1.0);
  	*(__m128d *)(d1 + ELEMENTSPERPACKET) = _mm_set1_pd(-1.0);
    *(__m128d *)(d1+ ELEMENTSPERPACKET * 2) = _mm_set1_pd(-1.0);\
      
    *(__m128d *) m1 = _mm_set1_pd(-1.0);
  	*(__m128d *)(m1 + ELEMENTSPERPACKET) = _mm_set1_pd(-1.0);
    *(__m128d *)(m1+ ELEMENTSPERPACKET * 2) = _mm_set1_pd(-1.0);

    // Data arrays a and b have not to be memory aligned to __m234 data (32 bytes)
    // so we use intermediate variables to avoid running errors.
    // We make an unaligned load of va and vb
    va = _mm_loadu_pd(pRcomp);      // va = a[0][1]â€¦[7] = 0, 1, 2, 3,  4,  5,  6,  7
    vb = _mm_loadu_pd(pRcomp2);      // vb = b[0][1]â€¦[7] = 0, 2, 4, 6,  8, 10, 12, 14
    vc = _mm_loadu_pd(c);

    *(__m128d *)r = _mm_sub_pd(vc, va);
		vr = _mm_loadu_pd(r);
    *(__m128d *)r2 = _mm_sub_pd(vc, vb);
    vr2 = _mm_loadu_pd(r2);
  	*(__m128d *)m1 = _mm_mul_pd(vr, vr2);
    vm1 = _mm_loadu_pd(m1);
    *(__m128d *)d1 = _mm_div_pd(vm1, vc);
    vd1 = _mm_loadu_pd(d1);
    *(__m128d *)r3 = _mm_sub_pd(vc, vd1);
    vr3 = _mm_loadu_pd(r3);
  
    // Next packet
    // va = a[8][9]â€¦[15] =  8,  9, 10, 11, 12, 13, 14, 15
    // vb = b[8][9]â€¦[15] = 16, 18, 20, 22, 24, 26, 28, 30
    //  c = c[8][9]â€¦[15] = 24, 27, 30, 33, 36, 39, 42, 45

    va = _mm_loadu_pd((pRcomp + ELEMENTSPERPACKET)); 
    vb = _mm_loadu_pd((pRcomp2 + ELEMENTSPERPACKET));
    vc = _mm_loadu_pd((c + ELEMENTSPERPACKET));
    vr  = _mm_loadu_pd((r + ELEMENTSPERPACKET));
    vr2 = _mm_loadu_pd((r2 + ELEMENTSPERPACKET));
    vm1 = _mm_loadu_pd((m1 + ELEMENTSPERPACKET));
    vd1 = _mm_loadu_pd((d1 + ELEMENTSPERPACKET));
    vr3 = _mm_loadu_pd((r3 + ELEMENTSPERPACKET));
    

    *(__m128d *)(r + ELEMENTSPERPACKET) = _mm_sub_pd(vc, va);
    *(__m128d *)(r2 + ELEMENTSPERPACKET) = _mm_sub_pd(vc, vb);
  	*(__m128d *)(m1 + ELEMENTSPERPACKET) = _mm_mul_pd(vr,vr2);
    *(__m128d *)(d1 + ELEMENTSPERPACKET) = _mm_div_pd(vm1,vc);
    *(__m128d *)(r3 + ELEMENTSPERPACKET) = _mm_sub_pd(vc,vd1);


    // Print resulting data from array addition
    for (int i = 0; i < TAM; i++)
 			printf("\nc[%d]: %d", i, *(c+i));
  
    _mm_free(r);
    _mm_free(r2);
    _mm_free(m1);
    _mm_free(d1);
    _mm_free(r3);  
}
/***********************************************
	 * End of the algorithm
	 *
	 * Measure the final time and calculate the time spent
	 *
	 * COMPLETE
	 *
	 */

	if (clock_gettime(CLOCK_REALTIME, &tEnd) == -1)
	{
		printf("ERROR: clock_gettime: %d.\n");
		exit(EXIT_FAILURE);
	}

	dElapsedTime = (tEnd.tv_sec - tStart.tv_sec);
	dElapsedTime += (tEnd.tv_nsec - tStart.tv_nsec) / 1e+9;
	printf("\nTime spent: %f s\n", dElapsedTime);

	// Create a new image object with the calculated pixels
	// In case of normal color image use nComp=3,
	// In case of B&W image use nComp=1.
	CImg<double> dstImage(pdstImage, width, height, 1, nComp);

	// Store the destination image in disk
	dstImage.save("output.bmp");

	// Display the destination image
	dstImage.display(); // If needed, show the result image
	return (0);
}