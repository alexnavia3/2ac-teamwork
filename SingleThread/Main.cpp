/*
 * Main.cpp
 *
 *  Created on: 13 sept. 2018
 *      Author: arias
 */

#include <CImg.h>
#include <math.h>
#include <stdio.h>
#include <errno.h>
#include <string.h>
using namespace cimg_library;

/**********************************
 * TODO
 * 	- Change the data type returned by CImg.srcImage to adjust the
 * 	requirements of your workgroup
 * 	- Change the data type of the components pointers to adjust the
 * 	requirements of your workgroup
 */

int main(int argc, char **argv)
{
	if (argc < 3)
	{
		printf("./main image1 image2\n");
		exit(EXIT_FAILURE);
	}

	CImg<double> srcImage(argv[1]);  // Open file and object initialization
	CImg<double> srcImage2(argv[2]); // Open file and object initialization

	double *pRcomp, *pGcomp, *pBcomp, *pRcomp2, *pGcomp2, *pBcomp2; // Pointers to the R, G and B components
	double *pRnew, *pGnew, *pBnew;
	double *pdstImage; // Pointer to the new image pixels
	int width, height; // Width and height of the image
	int nComp;		   // Number of image components
	struct timespec tStart, tEnd;
	double dElapsedTime;

	/***************************************************
	 *
	 * Variables initialization.
	 * Preparation of the necessary elements for the algorithm
	 * Out of the benchmark time
	 *
	 */

	srcImage.display(); // If needed, show the source image
	srcImage2.display();
	width = srcImage2.width(); // Getting information from the source image
	height = srcImage2.height();
	nComp = srcImage2.spectrum(); // source image number of components
								  // Common values for spectrum (number of image components):
								  //  B&W images = 1
								  //	Normal color images = 3 (RGB)
								  //  Special color images = 4 (RGB and alpha/transparency channel)

	// Allocate memory space for the pixels of the destination (processed) image
	pdstImage = (double *)malloc(width * height * nComp * sizeof(double));
	if (pdstImage == NULL)
	{
		printf("\nMemory allocating error\n");
		exit(-2);
	}

	// Pointers to the RGB arrays of the source image
	pRcomp = srcImage.data();		  // pRcomp points to the R component
	pGcomp = pRcomp + height * width; // pGcomp points to the G component
	pBcomp = pGcomp + height * width; // pBcomp points to B component

	pRcomp2 = srcImage2.data(); // pRcomp2 points to the R component
	pGcomp2 = pRcomp2 + height * width;
	pBcomp2 = pGcomp2 + height * width;

	// Pointers to the RGB arrays of the destination image
	pRnew = pdstImage;
	pGnew = pRnew + height * width;
	pBnew = pGnew + height * width;

	/*********************************************
	 * Algorithm start
	 *
	 * Measure initial time
	 *
	 *	COMPLETE
	 *
	 */
	printf("Runing task		: ");
	if (clock_gettime(CLOCK_REALTIME, &tStart) == -1)
	{
		printf("ERROR: clock_gettime: %d.\n", errno);
		exit(EXIT_FAILURE);
	}

	/************************************************
	 * Algorithm.
	 * In this example, the algorithm is a components exchange
	 *
	 * TO BE REPLACED BY YOUR ALGORITHM
	 */

	for (int j = 0; j < 20; j++)
	{
		for (int i = 0; i < (width * height); i++)
		{
			*(pRnew + i) = (255 - ((255 - *(pRcomp + i)) * (255 - *(pRcomp2 + i)) / 255));
			*(pGnew + i) = (255 - ((255 - *(pGcomp + i)) * (255 - *(pGcomp2 + i)) / 255));
			*(pBnew + i) = (255 - ((255 - *(pBcomp + i)) * (255 - *(pBcomp2 + i)) / 255));
		}
	}

	/***********************************************
	 * End of the algorithm
	 *
	 * Measure the final time and calculate the time spent
	 *
	 * COMPLETE
	 *
	 */

	if (clock_gettime(CLOCK_REALTIME, &tEnd) == -1)
	{
		printf("ERROR: clock_gettime: %d.\n", errno);
		exit(EXIT_FAILURE);
	}

	dElapsedTime = (tEnd.tv_sec - tStart.tv_sec);
	dElapsedTime += (tEnd.tv_nsec - tStart.tv_nsec) / 1e+9;
	printf("\nTime spent: %f s\n", dElapsedTime);

	// Create a new image object with the calculated pixels
	// In case of normal color image use nComp=3,
	// In case of B&W image use nComp=1.
	CImg<double> dstImage(pdstImage, width, height, 1, nComp);

	// Store the destination image in disk
	dstImage.save("output.bmp");

	// Display the destination image
	dstImage.display(); // If needed, show the result image
	return (0);
}
